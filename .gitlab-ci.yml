stages:
  - test
  - integration-test
  - build
  - deploy

# フロントエンドアプリケーションのテストを実施するジョブ
test-frontend:
  image: node:12.16.1-slim
  stage: test
  script:
    - .ci/test-frontend.sh
  artifacts:
    reports:
      junit:
        - frontend/junit.xml

# バックエンドアプリケーションの結合テストを実施するジョブ
integration-test-backend:
  image: maven:3.6.3-jdk-11-slim
  stage: integration-test
  variables:
    POSTGRES_PASSWORD: "example"
    NABLARCH_DB_URL: "jdbc:postgresql://postgres:5432/postgres"
    NABLARCH_LETTUCE_SIMPLE_URI: "redis://redis:6379"
    MAIL_SMTP_HOST: "mail"
    MAIL_SMTP_PORT: "1025"
    MAIL_SMTP_USER: "user"
    MAIL_SMTP_PASSWORD: "password"
    AWS_S3_ENDPOINTOVERRIDE: "http://minio:9000"
  services:
    - name: postgres:latest
    - name: redis:latest
    - name: minio/minio:latest
      alias: minio
      command: ["--compat", "server", "/data"]
    - name: mailhog/mailhog:latest
      alias: mail
  script:
    - .ci/integration-test-backend.sh
  artifacts:
    reports:
      junit:
        - backend/target/surefire-reports/TEST-*.xml
        - backend/target/failsafe-reports/TEST-*.xml

# 双方向通信用アプリケーションの結合テストを実施するジョブ
integration-test-notifier:
  image: maven:3.6.3-jdk-11-slim
  stage: integration-test
  variables:
    NABLARCH_LETTUCE_SIMPLE_URI: "redis://redis:6379"
  services:
    - name: redis:latest
  script:
    - .ci/integration-test-notifier.sh
  artifacts:
    reports:
      junit:
        - notifier/target/surefire-reports/TEST-*.xml
        - notifier/target/failsafe-reports/TEST-*.xml

# フロントエンドアプリケーションのビルドを実施するジョブ
build-frontend:
  image: node:12.16.1-slim
  stage: build
  only:
    refs:
      - master
  variables:
    REACT_APP_BACKEND_BASE_URL: $EPONA_REACT_APP_BACKEND_BASE_URL
  script:
    - .ci/build-frontend.sh
  artifacts:
    paths:
      - frontend/build

# バックエンドアプリケーションをコンテナとしてビルドするジョブ
# ここでは、コンテナイメージをGitLabのcontainer resistoryに登録する
build-backend:
  image: maven:3.6.3-jdk-11-slim
  stage: build
  only:
    refs:
      - master
  script:
    - .ci/build-backend.sh

# 双方向通信用アプリケーションをコンテナとしてビルドするジョブ
# ここでは、コンテナイメージをGitLabのcontainer resistoryに登録する
build-notifier:
  image: maven:3.6.3-jdk-11-slim
  stage: build
  only:
    refs:
      - master
  script:
    - .ci/build-notifier.sh

# AWSへデプロイするためのベースとなるジョブ定義
.deploy-aws:
  image: docker:19.03.13
  stage: deploy
  # タグとして `epona` がついているジョブは、GitLab Runnerで実行されます
  # Epona の提供する GitLab Runner からであれば credential をGitLabに登録せずにAWSリソースへのPUSHが可能になります
  tags:
    - epona
  variables:
    AWS_DEFAULT_REGION: $EPONA_AWS_DEFAULT_REGION
  services:
    - name: docker:19.03.13-dind
  only:
    refs:
      - master
  before_script:
    - .ci/install-awscliv2-on-alpine.sh

# GitLab container resistoryに登録されたバックエンドアプリケーションのコンテナイメージをECRにデプロイするジョブ
deploy-backend-to-ecr:
  extends: .deploy-aws
  variables:
    IMAGE_PUSHED_TO_ECR: $CI_REGISTRY_IMAGE/example-chat-backend:$CI_COMMIT_REF_NAME
    ECR_REPOSITORY: $EPONA_ECR_BASE_URI/$EPONA_ECR_BACKEND_REPOSITORY_NAME
  script:
    - .ci/deploy-to-ecr.sh

# GitLab container resistoryに登録された双方向通信用アプリケーションのコンテナイメージをECRにデプロイするジョブ
deploy-notifier-to-ecr:
  extends: .deploy-aws
  variables:
    IMAGE_PUSHED_TO_ECR: $CI_REGISTRY_IMAGE/example-chat-notifier:$CI_COMMIT_REF_NAME
    ECR_REPOSITORY: $EPONA_ECR_BASE_URI/$EPONA_ECR_NOTIFIER_REPOSITORY_NAME
  script:
    - .ci/deploy-to-ecr.sh

# ビルドしたフロントエンドアプリケーションをzip化してS3バケットへデプロイするジョブ
deploy-frontend-to-s3:
  extends: .deploy-aws
  variables:
    PUSH_TO_S3_BUCKET_NAME: $EPONA_FRONTEND_PUSH_TO_S3_BUCKET_NAME
    ARCHIVE_FILE_NAME: $EPONA_FRONTEND_ARCHIVE_FILE_NAME
  script:
    - .ci/deploy-to-s3.sh
  dependencies:
    - build-frontend

cache:
  key:
    files:
      - frontend/package-lock.json
      - backend/pom.xml
  policy: pull-push
  paths:
    - .npm
    - .m2
